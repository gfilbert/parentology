<?php
/*
 Soledad child theme functions and definitions
*/
function penci_soledad_child_scripts() {
    wp_enqueue_style( 'penci-soledad-parent-style', get_template_directory_uri(). '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'penci_soledad_child_scripts' );

/*
 * All custom functions go here
 */

function my_custom_sidebar() {
    register_sidebar(
        array (
            'name' => __( 'Below Content', 'Soledad-child' ),
            'id' => 'sidebar-below-content',
            'description' => __( 'This sidebar appears below post content.', 'Soledad-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'Above Content', 'Soledad-child' ),
            'id' => 'sidebar-above-content',
            'description' => __( 'This sidebar appears above post content.', 'Soledad-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'Above Next Button', 'Soledad-child' ),
            'id' => 'sidebar-above-next-btn',
            'description' => __( 'This sidebar appears above the "next" button on multipage posts.', 'Soledad-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'Custom Left Sidebar', 'Soledad-child' ),
            'id' => 'sidebar-custom-left',
            'description' => __( 'This sidebar appears to the left of main content.', 'Soledad-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'RMG Ad', 'Soledad-child' ),
            'id' => 'rmg',
            'description' => __( 'This sidebar appears above the post container on post pages.', 'Soledad-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'Below Content - mSpy', 'Soledad-child' ),
            'id' => 'sidebar-below-content-mspy',
            'description' => __( 'This sidebar appears below post content on style 10.', 'Soledad-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'Above Content - mSpy', 'Soledad-child' ),
            'id' => 'sidebar-above-content-mspy',
            'description' => __( 'This sidebar appears above post content on style 10.', 'Soledad-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'Above Next Button - mSpy', 'Soledad-child' ),
            'id' => 'sidebar-above-next-btn-mspy',
            'description' => __( 'This sidebar appears above the "next" button on multipage posts for style 10.', 'Soledad-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
}
add_action( 'widgets_init', 'my_custom_sidebar' );