<?php
/**
 * This is footer template of Soledad theme
 *
 * @package Wordpress
 * @since   1.0
 */

$penci_hide_footer = '';
$hide_fwidget = get_theme_mod( 'penci_footer_widget_area' );
$footer_layout = get_theme_mod( 'penci_footer_widget_area_layout' ) ? get_theme_mod( 'penci_footer_widget_area_layout' ) : 'style-1';
$penci_footer_width = get_theme_mod( 'penci_footer_width' );

if ( is_page() ) {
	$penci_hide_footer = get_post_meta( get_the_ID(), 'penci_page_hide_footer', true );

	$pmeta_page_footer = get_post_meta( get_the_ID(), 'penci_pmeta_page_footer', true );
	if ( isset( $pmeta_page_footer['penci_footer_style'] ) && $pmeta_page_footer['penci_footer_style'] ) {
		$footer_layout =  $pmeta_page_footer['penci_footer_style'];
	}

	if ( isset( $pmeta_page_footer['penci_hide_fwidget'] ) ) {
		if ( 'yes' == $pmeta_page_footer['penci_hide_fwidget'] ) {
			$hide_fwidget = true;
		} elseif ( 'no' == $pmeta_page_footer['penci_hide_fwidget'] ) {
			$hide_fwidget = false;
		}
	}

	if ( isset( $pmeta_page_footer['penci_footer_width'] ) && $pmeta_page_footer['penci_footer_width'] ) {
		$penci_footer_width =  $pmeta_page_footer['penci_footer_width'];
	}
}

$footer_logo_url = esc_url( home_url('/') );
if( get_theme_mod('penci_custom_url_logo_footer') ) {
	$footer_logo_url = get_theme_mod('penci_custom_url_logo_footer');
}
?>
<!-- END CONTAINER -->
</div>

<div class="clear-footer"></div>


				<?php if ( get_theme_mod( 'penci_footer_logo' ) && ! get_theme_mod( 'penci_hide_footer_logo' ) ) : ?>
					<div id="footer-logo">
						<a href="<?php echo $footer_logo_url; ?>">
							<img class="penci-lazy" src="<?php echo get_template_directory_uri(); ?>/images/penci2-holder.png" data-src="<?php echo esc_url( get_theme_mod( 'penci_footer_logo' ) ); ?>" alt="<?php esc_html_e( 'Footer Logo', 'soledad' ); ?>" />
						</a>
					</div>
				<?php endif; ?>

				<?php if ( penci_get_setting( 'penci_footer_copyright' ) ) : ?>
					<div id="footer-copyright">
						<p><?php echo penci_get_setting( 'penci_footer_copyright' ); ?></p>
					</div>
				<?php endif; ?>
			</div>
	</div>
</footer>

</div><!-- End .wrapper-boxed -->
<?php
$gprd_desc       = penci_get_setting( 'penci_gprd_desc' );
$gprd_accept     = penci_get_setting( 'penci_gprd_btn_accept' );
$gprd_rmore      = penci_get_setting( 'penci_gprd_rmore' );
$gprd_rmore_link = penci_get_setting( 'penci_gprd_rmore_link' );
$penci_gprd_text = penci_get_setting( 'penci_gprd_policy_text' );
if ( get_theme_mod( 'penci_enable_cookie_law' ) && $gprd_desc && $gprd_accept ) :
	?>
	<div class="penci-wrap-gprd-law penci-wrap-gprd-law-close penci-close-all">
		<div class="penci-gprd-law">
			<p>
				<?php if ( $gprd_desc ): echo $gprd_desc; endif; ?>
				<?php if ( $gprd_accept ): echo '<a class="penci-gprd-accept" href="#">' . $gprd_accept . '</a>'; endif; ?>
				<?php if ( $gprd_rmore ): echo '<a class="penci-gprd-more" href="' . $gprd_rmore_link . '">' . $gprd_rmore . '</a>'; endif; ?>
			</p>
		</div>
		<?php if ( ! get_theme_mod( 'penci_show_cookie_law' ) ): ?>
			<a class="penci-gdrd-show" href="#"><?php echo $penci_gprd_text; ?></a>
		<?php endif; ?>
	</div>

<?php endif; ?>
<?php wp_footer(); ?>

<?php if( get_theme_mod('penci_footer_analytics') ):
echo get_theme_mod('penci_footer_analytics');
endif; ?>
<?php
		if(isset($_GET['utm_source'])){
		switch (strtolower($_GET['utm_source'])) {
		case 'cad': 
?>
<style>
@media (max-width: 767px) {
#sticky-footer, div#unit, #parentology_728x90_970x90_468x60_320x50_300x50_adhesion {
display:none!important;
}
}
</style>
<?php
		break;
		case 'taboola':
?>
<style>
@media (max-width: 767px) {
#sticky-footer, div#unit, #parentology_728x90_970x90_468x60_320x50_300x50_adhesion {
display:none!important;
}
}
</style>
<?php
		break;
		case 'contentad_push':
?>
<style>
@media (max-width: 767px) {
#sticky-footer, div#unit, #parentology_728x90_970x90_468x60_320x50_300x50_adhesion {
display:none!important;
}
}
</style>
<?php
		break;
		case 'outbrain':
?>
<style>
@media (max-width: 767px) {
#sticky-footer, div#unit, #parentology_728x90_970x90_468x60_320x50_300x50_adhesion {
display:none!important;
}
}
</style>
<?php
		break;
		case 'fb_ads':
?>
<style>
@media (max-width: 767px) {
#sticky-footer, div#unit, #parentology_728x90_970x90_468x60_320x50_300x50_adhesion {
display:none!important;
}
}
</style>
<?php
		break;
		case 'google':
?>
<style>
@media (max-width: 767px) {
#sticky-footer, div#unit, #parentology_728x90_970x90_468x60_320x50_300x50_adhesion {
display:none!important;
}
}
</style>
<?php
		break;
		default:
?>
<style>
#sticky-footer, div#unit, #parentology_728x90_970x90_468x60_320x50_300x50_adhesion {
display:none!important;
}
</style>
<?php
		}
		}else{
?>
<style>
#sticky-footer, div#unit, #parentology_728x90_970x90_468x60_320x50_300x50_adhesion {
display:none!important;
}
</style>
<?php
		}
		?>

</body>
</html>