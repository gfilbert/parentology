<?php
/**
 * The Template for displaying all single posts
 *
 * @package Wordpress
 * @since   1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<?php if ( get_theme_mod( 'penci_favicon' ) ) : ?>
		<link rel="shortcut icon" href="<?php echo esc_url( get_theme_mod( 'penci_favicon' ) ); ?>" type="image/x-icon" />
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo esc_url( get_theme_mod( 'penci_favicon' ) ); ?>">
	<?php endif; ?>
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?> RSS Feed" href="<?php bloginfo( 'rss2_url' ); ?>" />
	<link rel="alternate" type="application/atom+xml" title="<?php bloginfo( 'name' ); ?> Atom Feed" href="<?php bloginfo( 'atom_url' ); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php
if( get_theme_mod( 'penci_custom_code_after_body_tag' ) ):
	echo do_shortcode( get_theme_mod( 'penci_custom_code_after_body_tag' ) );
endif;
?>
<?php
$penci_hide_header = $show_page_title = false;
if ( is_page() ) {
	$penci_hide_header = get_post_meta( get_the_ID(), 'penci_page_hide_header', true );
	
	$show_page_title = get_theme_mod( 'penci_pheader_show' );
	$penci_page_title = get_post_meta( get_the_ID(), 'penci_pmeta_page_title', true );

	$pheader_show = isset( $penci_page_title['pheader_show'] ) ? $penci_page_title['pheader_show'] : '';
	if(  'enable' == $pheader_show  ) {
		$show_page_title = true;
	}elseif(  'disable' == $pheader_show  ) {
		$show_page_title = false;
	}
}

/**
 * Get header layout in your customizer to change header layout
 *
 * @author PenciDesign
 */
$header_layout  = penci_soledad_get_header_layout();
$menu_style = get_theme_mod( 'penci_header_menu_style' ) ? get_theme_mod( 'penci_header_menu_style' ) : 'menu-style-1';

$header_class = $header_layout;
if( $header_layout == 'header-9' ) {
	$header_class = 'header-6 header-9';
}

if ( get_theme_mod( 'penci_vertical_nav_show' ) ) {
	get_template_part( 'template-parts/menu-hamburger' );
}
get_template_part( 'template-parts/header/vertical-nav' );

$class_wrapper_boxed = 'wrapper-boxed header-style-' . esc_attr( $header_layout );
if ( get_theme_mod( 'penci_body_boxed_layout' ) && ! get_theme_mod( 'penci_vertical_nav_show' ) ) {
	$class_wrapper_boxed .= ' enable-boxed';
}
if ( get_theme_mod( 'penci_enable_dark_layout' ) ) {
	$class_wrapper_boxed .= ' dark-layout-enabled';
}
if ( $penci_hide_header ) {
	$class_wrapper_boxed .= ' penci-page-hide-header';
}
if ( get_theme_mod( 'penci_header_logo_mobile_center' ) ) {
	$class_wrapper_boxed .= ' penci-hlogo-center';
}
?>
<div class="<?php echo esc_attr( $class_wrapper_boxed ); ?>">
<?php
if( ! $penci_hide_header ){

	echo '<div class="penci-header-wrap">';
	get_template_part( 'template-parts/header/top-instagram' );
	if( get_theme_mod( 'penci_top_bar_show' ) ){
		get_template_part( 'inc/modules/topbar' );
	}

	get_template_part( 'template-parts/header/' . $header_layout );
	echo '</div>';

	get_template_part( 'template-parts/header/mailchimp-below-header' );

	if( is_home() || get_theme_mod( 'penci_featured_slider_all_page' ) ) {
		get_template_part( 'template-parts/header/feature-slider' );
	}

	if( ( ( is_home() || is_front_page() ) && get_theme_mod( 'penci_signup_display_homepage' ) ) || ! get_theme_mod( 'penci_signup_display_homepage' ) ){
		get_template_part( 'template-parts/header/mailchimp-below-header2' );
	}
}
if ( $show_page_title && !is_home() && !is_front_page() ) {
	get_template_part( 'template-parts/page-header' );
}

$single_style = penci_get_single_style();

if ( ! in_array( $single_style, array( 'style-1', 'style-2' ) ) ) {
	get_template_part( 'template-parts/single', $single_style );
	return;
}

$sidebar_enable = penci_single_sidebar_return();
$sidebar_position = penci_get_posts_sidebar_class();
$sidebar_small_width = penci_single_smaller_content_enable();
?>

<?php if( ! get_theme_mod( 'penci_disable_breadcrumb' ) ): ?>
	<?php
	$yoast_breadcrumb = '';
	if ( function_exists( 'yoast_breadcrumb' ) ) {
		$yoast_breadcrumb = yoast_breadcrumb( '<div class="container penci-breadcrumb single-breadcrumb">', '</div>', false );
	}

	if( $yoast_breadcrumb ){
		echo $yoast_breadcrumb;
	}else{
		$yoast_breadcrumb = '';
		if ( function_exists( 'yoast_breadcrumb' ) ) {
			$yoast_breadcrumb = yoast_breadcrumb( '<div class="container penci-breadcrumb single-breadcrumb">', '</div>', false );
		}

		if( $yoast_breadcrumb ){
			echo $yoast_breadcrumb;
		 }else{ ?>
		<div class="container penci-breadcrumb single-breadcrumb">
			<span><a class="crumb" href="<?php echo esc_url( home_url('/') ); ?>"><?php echo penci_get_setting( 'penci_trans_home' ); ?></a></span><?php penci_fawesome_icon('fas fa-angle-right'); ?>
			<?php
			if ( get_theme_mod( 'enable_pri_cat_yoast_seo' ) ) {
				$primary_term = penci_get_wpseo_primary_term();

				if ( $primary_term ) {
					echo $primary_term;
				} else {
					$penci_cats = get_the_category( get_the_ID() );
					$penci_cat  = array_shift( $penci_cats );
					echo penci_get_category_parents( $penci_cat );
				}
			} else {
				$penci_cats = get_the_category( get_the_ID() );
				$penci_cat  = array_shift( $penci_cats );
				echo penci_get_category_parents( $penci_cat );
			}
			?>
			<span><?php the_title(); ?></span>
		</div>
		<?php } ?>
	<?php } ?>
<?php endif; ?>

<?php if ( 'style-2' == $single_style ) : ?>
	<div class="penci-single-pheader container container-single<?php if( get_theme_mod( 'penci_home_layout' ) == 'magazine-1' || get_theme_mod( 'penci_home_layout' ) == 'magazine-2' ): ?> container-single-magazine<?php endif;?><?php if ( $sidebar_enable ) { echo ' ' . esc_attr( $sidebar_position ); } else { echo ' penci_is_nosidebar'; } ?> container-single-fullwidth hentry">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'content', 'single-full' ); ?>
	<?php endwhile; endif; ?>
	</div>
<?php endif; ?>

<div class="container container-single<?php if( get_theme_mod( 'penci_home_layout' ) == 'magazine-1' || get_theme_mod( 'penci_home_layout' ) == 'magazine-2' ): ?> container-single-magazine<?php endif;?><?php if ( $sidebar_enable ) { ?> penci_sidebar <?php echo esc_attr( $sidebar_position ); ?><?php } else { echo ' penci_is_nosidebar'; } ?><?php if( $sidebar_small_width ): ?> penci-single-smaller-width<?php endif; ?><?php if( ! get_theme_mod( 'penci_disable_lightbox_single' ) ): ?> penci-enable-lightbox<?php endif; ?>">
	
	<div class="content-body-container">
	<div id="main"<?php if ( get_theme_mod( 'penci_sidebar_sticky' ) ): ?> class="penci-main-sticky-sidebar"<?php endif; ?>>
		<div class="theiaStickySidebar">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<?php /* Count viewed posts */ penci_set_post_views( $post->ID ); ?>
				<?php get_template_part( 'content', 'single' ); ?>
			<?php endwhile; endif; ?>
		</div>
	</div>
	<?php get_template_part( 'template-parts/single', 'sidebar' ); ?>
	
	</div>
<?php get_footer(); ?>