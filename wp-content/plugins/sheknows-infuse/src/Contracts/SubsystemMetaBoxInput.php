<?php

namespace SheKnows\Infuse\Contracts;

interface SubsystemMetaBoxInput {
	/**
	 * Renders input markup.
	 *
	 * @param \WP_Post $post
	 */
	public function render( $post );

	/**
	 * Saves post metadata.
	 *
	 * @param $post_id
	 */
	public function saveMetaData( $post_id );

	/**
	 * Checks the value of property for provided or current post.
	 *
	 * @param int|bool $post_id
	 *
	 * @return mixed
	 */
	public function getValue( $post_id = false );
}
