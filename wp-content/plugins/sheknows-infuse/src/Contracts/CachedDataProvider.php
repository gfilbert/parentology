<?php

namespace SheKnows\Infuse\Contracts;

interface CachedDataProvider {
	/**
	 * This is the method that data providers must implement in order to
	 * generate the data that they wish to be cached.  The return vaue MUST
	 * be an array with two elements: a 'success' boolean and a 'data' value.
	 * If 'success' is `true`, then the DataCache will cache the 'data' for the
	 * positive cache TTL.  If 'success' is `false`, then the DataCache will
	 * cache the data for the fail TTL if specified or the positive cache TTL
	 * if no fail TTL is specified.
	 *
	 * @param array $vars
	 * @return array ['success' => boolean, 'data' => mixed]
	 */
	public function generateData( $vars );
}
