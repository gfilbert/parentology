<?php

namespace SheKnows\Infuse\Core;

/**
 * Class Activation
 *
 * @package SheKnows\Infuse\Core
 */
class Activation {
	/**
	 * @var Options
	 */
	protected $options;

	/**
	 * @var Tracking
	 */
	protected $tracking;

	/**
	 * Activation constructor.
	 *
	 * @param Options  $options
	 * @param Tracking  $tracking
	 */
	public function __construct( Options $options, Tracking $tracking ) {
		$this->options  = $options;
		$this->tracking = $tracking;
	}

	/**
	 * Called when administrator activates the plugin.
	 */
	public function activate() {
		$this->tracking->trackEvent( 'activate' );
	}

	/**
	 * Called when plugin is deactivated.
	 */
	public function deactivate() {
		$this->tracking->trackEvent( 'deactivate' );

		// unschedule pings
		wp_clear_scheduled_hook( 'sheknows_infuse_track_event', array( 'ping' ) );
		// Remove the saved options from the database.
		$this->options->clear();
	}
}
