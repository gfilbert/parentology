<?php

namespace SheKnows\Infuse\Core;

/**
 * Retrieves and provides access to a config object from one of the upstream
 * config files.
 *
 * @package SheKnows\Infuse\Core
 */
class UpstreamConfig {
	/**
	 * The CacheBoomerangFile instance.
	 *
	 * @var CacheBoomerangFile
	 */
	protected $cache;

	/**
	 * The retrieved config object.
	 *
	 * @var Array
	 */
	protected $config;

	/**
	 * Constructor.
	 *
	 * @param CacheBoomerangFile $cache
	 */
	public function __construct( CacheBoomerangFile $cache ) {
		$this->cache = $cache;
	}

	/**
	 * Attempt to hydrate our config options from the upstream file.
	 */
	protected function load( $force ) {
		if ( ! isset( $this->config ) || $force ) {
			$response = $this->cache->get( $force );

			if ( false !== $response ) {
				$config = json_decode( $response['content'], true );
				if ( is_array( $config ) ) {
					$this->config = $config;
				}
			}
		}
	}

	/**
	 * Retrieves a specific config value.
	 *
	 * @param  string      $name   The name of the config value to retrieve.
	 * @param  boolean     $force  Set to true if the caller would like to try
	 *                             to retrieve a fresh copy of the upstream
	 *                             config.
	 * @return mixed               The value of the config item.
	 * @throws \Exception          If we are unable to retrieve the upstream
	 *                             config, and no cache was available, we have a
	 *                             serious problem, so we throw in that case.
	 *                             Callers of this method are expected to catch
	 *                             this exception and handle it gracefully.
	 */
	public function get( $name, $force = false ) {
		$this->load( $force );

		if ( ! isset( $this->config ) ) {
			throw new \Exception( 'Unable to retrieve upstream config from ' . $this->cache->getUrl() );
		}

		if ( ! isset( $this->config[ $name ] ) ) {
			return false;
		}

		return $this->config[ $name ];
	}

	/**
	 * Forces retrieval the config values.
	 */
	public function forceRetrieve() {
		$this->load( true );
	}
}
