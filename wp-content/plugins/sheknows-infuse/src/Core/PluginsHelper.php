<?php

namespace SheKnows\Infuse\Core;

class PluginsHelper {

	/**
	 * Includes helper functions if needs.
	 */
	public function __construct() {
		if ( ! function_exists( 'get_plugins' ) ) {
			require_once ABSPATH . '/wp-admin/includes/plugin.php';
		}
	}

	/**
	 * Filter active plugins by their slugs and returns slug=>info array
	 *
	 * @param array $slugs
	 *
	 * @return array
	 */
	public function filterActiveBySlugs( $slugs ) {
		$plugins = array();
		$slugs   = array_filter(
			$slugs,
			function ( $slug ) {
				return is_plugin_active( $slug );
			}
		);
		foreach ( $slugs as $plugin ) {
			$plugins[ $plugin ] = get_plugin_data( WP_PLUGIN_DIR . '/' . $plugin );
		}

		return $plugins;
	}
}

