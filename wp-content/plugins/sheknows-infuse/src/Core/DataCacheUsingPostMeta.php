<?php
namespace SheKnows\Infuse\Core;

use SheKnows\Infuse\Contracts\CachedDataProvider;

/**
 * Caches data based on responses from a data provider function.
 *
 * @package SheKnows\Infuse\Core
 */
class DataCacheUsingPostMeta extends DataCache {

	const META_PREFIX = '_sheknows_infuse_cache_';

	private $metaName = null;

	/**
	 * Flag to enable serving stale cache, default to true for now.
	 * @var bool
	 */
	protected $serveStaleCache = true;

	/**
	 * Get the cached data if present, otherwise generate the data, cache it, then return it.
	 *
	 * @return mixed
	 */
	public function get() {

		$postId = $this->getPostId();

		if ( empty( $postId ) ) {
			return parent::get();
		}

		$cacheData = get_post_meta( $postId, $this->cacheKey(), true );

		// Check for empty or invalid cache data
		if ( empty( $cacheData )
			|| ! is_array( $cacheData )
			|| ! isset( $cacheData['expiration'] )
			|| ! isset( $cacheData['data'] )
		) {
			if ( isset( $this->defaultDataNotReady ) ) {
				$this->scheduleFetch();
				return $this->defaultDataNotReady;
			}
		} else {
			if ( $cacheData['expiration'] < time() ) {
				if ( ! empty( $this->serveStaleCache ) ) {
					// IMPORTANT: we want to continue serving the stale cache without being blocked by cache refresh
					// cache has expired, we need to schedule the data fetch to refresh at shutdown event
					$this->scheduleFetch();
					return $cacheData['data'];
				}
			} else {
				return $cacheData['data'];
			}
		}

		return $this->fetchData();
	}

	/**
	 * Update the cache content using cache ttl base on if content failed or succeed
	 * @param array $response
	 */
	protected function updateCache( $response ) {

		$postId = $this->getPostId();

		if ( empty( $postId ) ) {
			parent::updateCache( $response );
		} else {

			$cacheKey = $this->cacheKey();

			$cacheData = get_post_meta( $postId, $cacheKey, true );

			// Reset cache if data is invalid
			if ( empty( $cacheData ) || ! is_array( $cacheData ) ) {
				$cacheData = array(
					'failed_count'  => 0,
					'success_count' => 0,
				);
			}

			// We want to serve the existing stale cache if current content failed to retrieved from remote
			// if serveStaleCache is not set, we do not support serving stale data, so we can proceed with updating cache as is
			if ( empty( $this->serveStaleCache ) || empty( $cacheData['data'] ) || empty( $cacheData['success'] ) || $response['success'] ) {
				$cacheData['data']         = $response['data'];
				$cacheData['success']      = $response['success'];
				$cacheData['last_updated'] = time();
			}

			$ttl = $this->cacheTTL;
			if ( ! $response['success'] ) {
				if ( ! empty( $this->failCacheTTL ) ) {
					$ttl = $this->failCacheTTL;
				}
				if ( empty( $cacheData['first_failed'] ) ) {
					$cacheData['first_failed'] = time();
				}
				$cacheData['failed_count'] = (int) $cacheData['failed_count'] + 1;
				$cacheData['last_failed']  = time();
			} else {
				if ( empty( $cacheData['first_success'] ) ) {
					$cacheData['first_success'] = time();
				}
				$cacheData['success_count'] = (int) $cacheData['success_count'] + 1;
			}

			$cacheData['expiration'] = time() + $ttl;  // ttl in seconds

			update_post_meta( $postId, $cacheKey, $cacheData );

		}
	}

	/**
	 * Wrapper function to generate the cache key
	 * @return string
	 */
	protected function cacheKey() {
		if ( empty( $this->metaName ) ) {
			$this->metaName = self::META_PREFIX . md5( get_class( $this->dataProvider ) );
		}
		return $this->metaName;
	}

	/**
	 * Return the current postId if passed
	 * @return mixed
	 */
	protected function getPostId() {
		if ( ! empty( $this->dataProviderArguments['postId'] ) ) {
			return $this->dataProviderArguments['postId'];
		}
		return false;
	}

}
