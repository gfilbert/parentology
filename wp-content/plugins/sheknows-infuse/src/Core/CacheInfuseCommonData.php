<?php

namespace SheKnows\Infuse\Core;

/**
 * Checks and updates cached infuse-common-data.json.
 *
 * @package SheKnows\Infuse\Core
 */
class CacheInfuseCommonData extends CacheBoomerangFile {
	/**
	 * Portion of the transient name that uniquely identifies this cached file.
	 * Subclasses should set this.
	 *
	 * @var string
	 */
	protected $transientName = 'infusecommondata';

	/**
	 * {@inheritdoc}
	 */
	public function getUrl() {
		$baseUrl = $this->getBaseUrl();

		return $baseUrl . '/static/infuse-common-data.json';
	}
}
