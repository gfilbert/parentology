<?php

namespace SheKnows\Infuse\Core;

/**
 * Retrieves and provides access to the upstream config options from the site's infuse.json file.
 * If the option not found, fallback to the default from the common upstream config options.
 *
 * @package SheKnows\Infuse\Core
 */
class Config {

	/**
	 * @var UpstreamSiteConfig
	 */
	protected $siteConfig;

	/**
	 * @var UpstreamCommonConfig
	 */
	protected $commonConfig;

	/**
	 * Config constructor.
	 *
	 * @param UpstreamSiteConfig $siteConfig
	 * @param UpstreamCommonConfig $commonConfig
	 */
	public function __construct( UpstreamSiteConfig $siteConfig, UpstreamCommonConfig $commonConfig ) {
		$this->siteConfig   = $siteConfig;
		$this->commonConfig = $commonConfig;
	}

	/**
	 * Retrieves a specific site config value; if value is empty/false then fallback to retrieve from common config
	 *
	 * @param  string      $name       The name of the config value to retrieve.
	 * @param  string      $commonName The name of the common config value to retrieve; fallback to $name if not specified
	 * @param  boolean     $force      Set to true if the caller would like to try
	 *                                 to retrieve a fresh copy of the upstream
	 *                                 config.
	 * @return mixed                   The value of the config item.
	 * @throws \Exception              If we are unable to retrieve the upstream
	 *                                 config, and no cache was available, we have a
	 *                                 serious problem, so we throw in that case.
	 *                                 Callers of this method are expected to catch
	 *                                 this exception and handle it gracefully.
	 */
	public function get( $name, $commonName = false, $force = false ) {

		$value = $this->siteConfig->get( $name, $force );
		if ( false === $value ) {
			if ( empty( $commonName ) ) {
				$commonName = $name;
			}
			$value = $this->commonConfig->get( $commonName, $force );
		}

		return $value;
	}
}
