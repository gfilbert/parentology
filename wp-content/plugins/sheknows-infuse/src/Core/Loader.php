<?php

namespace SheKnows\Infuse\Core;

/**
 * Class Loader
 *
 * @package SheKnows\Infuse\Core
 */
class Loader {

	/**
	 * The IoC container.
	 *
	 * @var Container
	 */
	protected $container;

	/**
	 * The base directory for the plugin subsystems.
	 *
	 * @var string
	 */
	protected $subsystemDir;

	/**
	 * Loader constructor.
	 *
	 * @param Container $container
	 * @param string    $subsystemDir
	 */
	public function __construct( Container $container, $subsystemDir ) {
		$this->container    = $container;
		$this->subsystemDir = $subsystemDir;
	}

	/**
	 * Initializes loading core classes and binding core WordPress API hooks
	 */
	public function load() {
		$this->bindCoreClasses();
		$this->registerCoreHooks();
		$this->loadSubsystems();
		$this->maybeFirePing();
	}

	/**
	 * Makes core classes available at runtime. If class has not been
	 * instantiated, the class is instantiated (with dependencies added through
	 * the constructor) and handed over. If the class has been instantiated,
	 * the class is handed over.
	 */
	protected function bindCoreClasses() {
		$this->container->bind( __NAMESPACE__ . '\Options', array( $this, 'bindOptions' ) );
		$this->container->bind( __NAMESPACE__ . '\ErrorLogging', array( $this, 'bindErrorLogging' ) );
		$this->container->bind( __NAMESPACE__ . '\Activation', array( $this, 'bindActivation' ) );
		$this->container->bind( __NAMESPACE__ . '\Admin', array( $this, 'bindAdmin' ) );
		$this->container->bind( __NAMESPACE__ . '\ViewFactory', array( $this, 'bindViewFactory' ) );
		$this->container->bind( __NAMESPACE__ . '\AdTypes', array( $this, 'bindAdTypes' ) );
		$this->container->bind( __NAMESPACE__ . '\CacheInfuseJson', array( $this, 'bindCacheInfuseJson' ) );
		$this->container->bind( __NAMESPACE__ . '\CacheInfuseCommonData', array( $this, 'bindCacheInfuseCommonData' ) );
		$this->container->bind( __NAMESPACE__ . '\UpstreamCommonConfig', array( $this, 'bindUpstreamCommonConfig' ) );
		$this->container->bind( __NAMESPACE__ . '\UpstreamSiteConfig', array( $this, 'bindUpstreamSiteConfig' ) );
		$this->container->bind( __NAMESPACE__ . '\UpstreamServiceStatus', array( $this, 'bindUpstreamServiceStatus' ) );
		$this->container->bind( __NAMESPACE__ . '\Tracking', array( $this, 'bindTracking' ), 'SubsystemTracker' );
		$this->container->bind( __NAMESPACE__ . '\PluginsHelper', array( $this, 'bindPluginsHelper' ) );
		$this->container->bind( __NAMESPACE__ . '\MetaBox', array( $this, 'bindMetaBox' ) );
		$this->container->bind( __NAMESPACE__ . '\Decryptor', array( $this, 'bindDecryptor' ) );
		$this->container->bind( __NAMESPACE__ . '\DataCacheFactory', array( $this, 'bindDataCacheFactory' ) );
		$this->container->bind( __NAMESPACE__ . '\Config', array( $this, 'bindConfig' ) );
	}

	/**
	 * Constructs Options class
	 *
	 * @return Options
	 */
	public function bindOptions() {
		$plugin = $this->container->resolve( 'SheKnows\Infuse\Plugin' );

		return new Options( $plugin );
	}

	/**
	 * Constructs ErrorLogging class
	 *
	 * @return ErrorLogging
	 */
	public function bindErrorLogging() {
		return new ErrorLogging();
	}

	/**
	 * Constructs Activation class
	 *
	 * @return Activation
	 */
	public function bindActivation() {
		$options  = $this->container->resolve( __NAMESPACE__ . '\Options' );
		$tracking = $this->container->resolve( __NAMESPACE__ . '\Tracking' );

		return new Activation( $options, $tracking );
	}

	/**
	 * Constructs PluginsHelper class
	 *
	 * @return PluginsHelper
	 */
	public function bindPluginsHelper() {
		return new PluginsHelper();
	}

	/**
	 * Constructs Tracking class.
	 *
	 * @return Tracking
	 */
	public function bindTracking() {
		$options = $this->container->resolve( __NAMESPACE__ . '\Options' );
		$plugin  = $this->container->resolve( 'SheKnows\Infuse\Plugin' );

		return new Tracking( $options, $plugin, $this->container );
	}

	/**
	 * Constructs Admin class
	 *
	 * @return Admin
	 */
	public function bindAdmin() {
		$options      = $this->container->resolve( __NAMESPACE__ . '\Options' );
		$plugin       = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$viewFactory  = $this->container->resolve( __NAMESPACE__ . '\ViewFactory' );
		$siteConfig   = $this->container->resolve( __NAMESPACE__ . '\UpstreamSiteConfig' );
		$errorLogging = $this->container->resolve( __NAMESPACE__ . '\ErrorLogging' );

		return new Admin( $options, $plugin, $viewFactory, $this->container, $siteConfig, $errorLogging );
	}

	/**
	 * Binds the AdTypes resolver.
	 *
	 * @return AdTypes
	 */
	public function bindAdTypes() {
		$config       = $this->container->resolve( __NAMESPACE__ . '\Config' );
		$errorLogging = $this->container->resolve( __NAMESPACE__ . '\ErrorLogging' );
		return new AdTypes( $config, $errorLogging );
	}

	/**
	 * Constructs ViewFactory class
	 *
	 * @return ViewFactory
	 */
	public function bindViewFactory() {
		$plugin = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$base   = dirname( $plugin->getFile() ) . '/views';

		return new ViewFactory( $base );
	}

	/**
	 * Constructs DataCacheFactory class
	 *
	 * @return DataCacheFactory
	 */
	public function bindDataCacheFactory() {
		$errorLogging = $this->container->resolve( __NAMESPACE__ . '\ErrorLogging' );

		return new DataCacheFactory( $errorLogging );
	}

	/**
	 * Constructs the CacheInfuseJson class.
	 *
	 * @return CacheInfuseJson
	 */
	public function bindCacheInfuseJson() {
		$options = $this->container->resolve( __NAMESPACE__ . '\Options' );

		return new CacheInfuseJson( $options );
	}

	/**
	 * Constructs the CacheInfuseCommonData class.
	 *
	 * @return CacheInfuseCommonData
	 */
	public function bindCacheInfuseCommonData() {
		$options = $this->container->resolve( __NAMESPACE__ . '\Options' );

		return new CacheInfuseCommonData( $options );
	}

	/**
	 * Constructs the UpstreamCommonConfig class.
	 *
	 * @return UpstreamCommonConfig
	 */
	public function bindUpstreamCommonConfig() {
		$commonDataCache = $this->container->resolve( __NAMESPACE__ . '\CacheInfuseCommonData' );

		return new UpstreamCommonConfig( $commonDataCache );
	}

	/**
	 * Constructs the UpstreamSiteConfig class.
	 *
	 * @return UpstreamSiteConfig
	 */
	public function bindUpstreamSiteConfig() {
		$jsonCache = $this->container->resolve( __NAMESPACE__ . '\CacheInfuseJson' );

		return new UpstreamSiteConfig( $jsonCache );
	}

	/**
	 * Constructs Config class.
	 *
	 * @return Config
	 */
	public function bindConfig() {
		$siteConfig   = $this->container->resolve( __NAMESPACE__ . '\UpstreamSiteConfig' );
		$commonConfig = $this->container->resolve( __NAMESPACE__ . '\UpstreamCommonConfig' );

		return new Config( $siteConfig, $commonConfig );
	}


	/**
	 * Constructs the UpstreamServiceStatus class.
	 *
	 * @return UpstreamServiceStatus
	 */
	public function bindUpstreamServiceStatus() {
		$commonConfig = $this->container->resolve( __NAMESPACE__ . '\UpstreamCommonConfig' );
		$siteConfig   = $this->container->resolve( __NAMESPACE__ . '\UpstreamSiteConfig' );
		$errorLogging = $this->container->resolve( __NAMESPACE__ . '\ErrorLogging' );

		return new UpstreamServiceStatus( $commonConfig, $siteConfig, $errorLogging );
	}

	/**
	 * Constructs the MetaBox class.
	 *
	 * @return MetaBox
	 */
	public function bindMetaBox() {
		$plugin = $this->container->resolve( 'SheKnows\Infuse\Plugin' );

		return new MetaBox( $this->container, $plugin );
	}

	/**
	 * Constructs the Decryptor class.
	 *
	 * @return Decryptor
	 */
	public function bindDecryptor() {
		$options = $this->container->resolve( __NAMESPACE__ . '\Options' );

		return new Decryptor( $options );
	}

	/**
	 * Loads the Activation class and runs the activation when triggered.
	 */
	public function handleActivation() {
		$activation = $this->container->resolve( __NAMESPACE__ . '\Activation' );
		$activation->activate();
	}

	/**
	 * Loads the Activation class and runs the deactivation when triggered.
	 */
	public function handleDeactivation() {
		$activation = $this->container->resolve( __NAMESPACE__ . '\Activation' );
		$activation->deactivate();
	}

	/**
	 * Adds core WordPress hooks to the WordPress API including activation, database
	 * check on each page load, and if needed the admin settings page
	 */
	protected function registerCoreHooks() {
		$plugin = $this->container->resolve( 'SheKnows\Infuse\Plugin' );

		register_activation_hook( $plugin->getFile(), array( $this, 'handleActivation' ) );
		register_deactivation_hook( $plugin->getFile(), array( $this, 'handleDeactivation' ) );

		add_action( 'sheknows_infuse_track_event', array( $this, 'handleScheduledTracker' ) );
		add_action( 'updated_option', array( $this, 'handleUpdatedOption' ) );

		if ( is_admin() ) {
			add_filter(
				'plugin_action_links_' . plugin_basename( $plugin->getFile() ),
				array( $this, 'addPluginPageLinks' )
			);
			add_action( 'admin_menu', array( $this, 'addSettingsPage' ) );
			add_action( 'admin_init', array( $this, 'registerSettings' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueueAdminAssets' ) );
			add_action( 'load-settings_page_' . $plugin->getSlug(), array( $this, 'addSettingsPageHelpers' ) );
			add_action( 'admin_notices', array( $this, 'addAdminNags' ) );
			$this->addMetaBoxHooks();
		} else {
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueuePublicAssets' ) );
		}
	}

	/**
	 * If there isn't a ping hook scheduled, schedule one.
	 */
	protected function maybeFirePing() {
		$ping = array( 'ping' );

		if ( wp_next_scheduled( 'sheknows_infuse_track_event', $ping ) === false ) {
			wp_schedule_event( strtotime( '+1 day' ), 'daily', 'sheknows_infuse_track_event', $ping );
		}
	}

	/**
	 * Handle a scheduled tracking call.
	 *
	 * @param string $eventName Name of tracked event.
	 */
	public function handleScheduledTracker( $eventName ) {
		$tracking = $this->container->resolve( __NAMESPACE__ . '\Tracking' );

		$tracking->trackEvent( $eventName );
	}

	/**
	 * Listen and react to our options being updated.
	 *
	 * @param  string $option The option that was updated.
	 */
	public function handleUpdatedOption( $option ) {
		$options = $this->container->resolve( __NAMESPACE__ . '\Options' );
		$prefix  = $options->getStoredName();

		if ( 0 === strpos( $option, $prefix ) ) {
			// Schedule a tracking call to occur at most once every 30 minutes when
			// an option belonging to us is updated.  Note that WordPress ignores
			// single event schedule requests that happen within 10 minutes after an
			// existing scheduled event, hence the 30 minute throttle.
			$schedule = time() + ( 20 * MINUTE_IN_SECONDS );
			wp_schedule_single_event( $schedule, 'sheknows_infuse_track_event', array( 'settings' ) );
		}
	}

	/**
	 * Prepend a link to the Options page to the Infuse links on the plugins
	 * page.
	 *
	 * @param array $links The list of links already added.
	 *
	 * @return array
	 */
	public function addPluginPageLinks( $links ) {
		$plugin       = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$settingsLink = sprintf( '<a href="%s">Settings</a>', esc_url( $plugin->getOptionsPageUrl() ) );

		return array_merge( array( $settingsLink ), $links );
	}

	/**
	 * Captures hook for adding options page
	 */
	public function addSettingsPage() {
		$plugin    = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$pageTitle = __( 'SHE Media Infuse Settings', 'sheknows-infuse' );
		$linkText  = __( 'SHE Media Infuse', 'sheknows-infuse' );

		add_options_page(
			$pageTitle,
			$linkText,
			'manage_options',
			$plugin->getSlug(),
			array( $this, 'displaySettingsPage' )
		);
	}

	/**
	 * Add admin nags on all admin pages EXCEPT for our own settings page.
	 */
	public function addAdminNags() {
		$plugin       = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$settingsPage = 'settings_page_' . $plugin->getSlug();

		// Don't display our nags on our own settings screen or on admin pages
		// where get_current_screen() doesn't exist.
		if ( ! function_exists( 'get_current_screen' ) || get_current_screen()->id === $settingsPage ) {
			return;
		}

		$options         = $this->container->resolve( __NAMESPACE__ . '\Options' );
		$boomerangPath   = $options->get( 'boomerang_path' );
		$infuseSharedKey = $options->get( 'infuse_shared_key' );
		$class           = 'notice notice-warning is-dismissible';

		if ( empty( $boomerangPath ) ) {
			$link = sprintf( '<a href="%s">visit the settings page</a>', esc_url( $plugin->getOptionsPageUrl() ) );

			// translators: Prompt to configure Infuse.
			$message = esc_html__( 'SHE Media Infuse has been installed but is not yet configured.  Please %s to configure Infuse.', 'sheknows-infuse' );
			$message = apply_filters( 'sheknows_infuse_not_configured_message', $message );

			// phpcs:ignore WordPress.Security.EscapeOutput
			printf( '<div class="%s"><p>%s</p></div>', esc_attr( $class ), sprintf( $message, $link ) );
		} elseif ( empty( $infuseSharedKey ) ) {
			$admin       = $this->container->resolve( __NAMESPACE__ . '\Admin' );
			$url         = $admin->buildAutoconfigLink( $boomerangPath );
			$buttonClass = 'button button-primary button-hero';
			$link        = sprintf( '<a class="%s" href="%s">Complete SHE Media Infuse Configuration</a>', esc_attr( $buttonClass ), esc_url( $url ) );
			$link        = apply_filters( 'sheknows_infuse_not_configured_message', $link );

			$message = sprintf(
				'<p><span class="dashicons dashicons-warning"></span>%s</p>%s',
				__( 'SHE Media Infuse is not fully configured.', 'sheknows-infuse' ),
				$link
			);

			// phpcs:ignore WordPress.Security.EscapeOutput
			printf( '<div class="%s"><p>%s</p></div>', esc_attr( $class ), $message );
		}
	}

	/**
	 * Forces infuse JSON refresh
	 */
	protected function forceInfuseJSONRefresh() {
		$jsonCache = $this->container->resolve( __NAMESPACE__ . '\CacheInfuseJson' );
		$jsonCache->get( true );
	}

	/**
	 * Initial display of settings page
	 */
	public function displaySettingsPage() {
		$this->forceInfuseJSONRefresh();

		$admin = $this->container->resolve( __NAMESPACE__ . '\Admin' );
		$admin->displaySettingsPage();
	}

	/**
	 * Fills out settings page form
	 */
	public function registerSettings() {
		$admin = $this->container->resolve( __NAMESPACE__ . '\Admin' );
		$admin->registerSettings();
	}

	/**
	 * Add a nag on admin pages if Infuse has been activated but does not have a
	 * Boomerang path configured.
	 */
	public function addSettingsNag() {
		$plugin = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$class  = 'notice notice-warning is-dismissible';
		$link   = sprintf( '<a href="%s">visit the settings page</a>', esc_url( $plugin->getOptionsPageUrl() ) );

		// translators: Prompt to configure Infuse.
		$message = esc_html__( 'SHE Media Infuse has been installed but is not yet configured.  Please %s to configure Infuse.', 'sheknows-infuse' );
		$message = apply_filters( 'sheknows_infuse_not_configured_message', $message );

		// phpcs:ignore WordPress.Security.EscapeOutput
		printf( '<div class="%s"><p>%s</p></div>', esc_attr( $class ), sprintf( $message, $link ) );
	}

	/**
	 * Add a nag on admin pages if Infuse has been activated and Boomerang path configured
	 * but Infuse Shared Key has not been stored.
	 */
	public function addInfuseSharedKeyNag() {
		$admin   = $this->container->resolve( __NAMESPACE__ . '\Admin' );
		$options = $this->container->resolve( __NAMESPACE__ . '\Options' );

		$adPath = $options->get( 'boomerang_path' );
		$class  = 'notice notice-warning is-dismissible';
		$url    = $admin->buildAutoconfigLink( $adPath );
		$link   = sprintf( '<a href="%s">click here</a>', esc_url( $url ) );

		// translators: Prompt to configure Infuse.
		$message = esc_html__( 'SHE Media Infuse is not fully configured. Please %s to complete the configuration.', 'sheknows-infuse' );

		// phpcs:ignore WordPress.Security.EscapeOutput
		printf( '<div class="%s"><p>%s</p></div>', esc_attr( $class ), sprintf( $message, $link ) );
	}

	/**
	 * Enqueue our admin styles/scripts for the Infuse options page.
	 *
	 * @param string $hook The hook name of the current admin page.
	 */
	public function enqueueAdminAssets( $hook ) {
		$plugin         = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$ourOptionsHook = 'settings_page_' . $plugin->getSlug();

		if ( $hook === $ourOptionsHook ) {
			wp_enqueue_style( $plugin->getSlug() . '-admin', plugin_dir_url( $plugin->getFile() ) . 'public/css/admin.css', array(), $plugin->getVersion() );
			wp_enqueue_script( $plugin->getSlug() . '-admin', plugin_dir_url( $plugin->getFile() ) . 'public/js/sheknows-infuse-admin.js', array(), $plugin->getVersion(), false );
		} else {
			wp_enqueue_script( $plugin->getSlug() . '-admin', plugin_dir_url( $plugin->getFile() ) . 'public/js/sheknows-infuse-admin-global.js', array(), $plugin->getVersion(), false );
		}
	}

	/**
	 * Enqueue our styles/scripts for the public pages.
	 */
	public function enqueuePublicAssets() {
		$plugin = $this->container->resolve( 'SheKnows\Infuse\Plugin' );

		wp_enqueue_style( $plugin->getSlug(), plugin_dir_url( $plugin->getFile() ) . 'public/css/style.css', array(), $plugin->getVersion() );
		// phpcs:ignore Squiz.PHP.CommentedOutCode.Found
		// Uncomment this once we have frontend JS to include.
		// wp_enqueue_script( $plugin->getSlug(), plugin_dir_url( $plugin->getFile() ) . 'public/js/sheknows-infuse.js', array(), $plugin->getVersion() );
	}

	/**
	 * Extra functionality loaded when the settings page is loaded.
	 */
	public function addSettingsPageHelpers() {
		// Filter out one-time-use params when rewriting the URL.
		$admin  = $this->container->resolve( __NAMESPACE__ . '\Admin' );
		$params = $admin->getOneTimeQueryArgs();
		add_filter(
			'removable_query_args',
			function ( $args ) use ( $params ) {
				return array_merge( $args, $params );
			}
		);
	}

	/**
	 * Call the Loaders for all of the Subsystems.
	 */
	protected function loadSubsystems() {
		$subsystems = scandir( $this->subsystemDir );

		foreach ( $subsystems as $name ) {
			$fullPath = sprintf( '%s/%s', $this->subsystemDir, $name );

			if ( is_dir( $fullPath ) && file_exists( $fullPath . '/Loader.php' ) ) {
				// Note: the Loaders themselves are not actually bound in or
				// resolved from the Container, since their purpose is solely
				// for bootstrapping the plugin and loading their subsystems'
				// classes into the Container.
				$class  = "\\SheKnows\\Infuse\\Subsystems\\$name\\Loader";
				$loader = new $class( $this->container );
				$loader->load();
			}
		}
	}

	/**
	 * Adds hooks for metabox inputs functionality.
	 */
	protected function addMetaBoxHooks() {
		$postTypes = $this->getPublicPostTypes();
		$container = $this->container;

		foreach ( $postTypes as $type ) {
			add_action(
				'add_meta_boxes',
				function () use ( $type, $container ) {
					$metaBox = $container->resolve( __NAMESPACE__ . '\MetaBox' );
					$metaBox->addMetaBox( $type );
				}
			);
			add_action(
				'save_post_' . $type,
				function ( $post_id ) use ( $container ) {
					$metaBox = $container->resolve( __NAMESPACE__ . '\MetaBox' );
					$metaBox->saveMetaData( $post_id );
				}
			);
		}
	}

	/**
	 * Returns types of public content.
	 *
	 * @return array|string
	 */
	protected function getPublicPostTypes() {
		return array_values( get_post_types( array( 'public' => true ) ) );
	}
}
