<?php

namespace SheKnows\Infuse\Core;

/**
 * Retrieves and provides access to the upstream config options from the site's
 * infuse.json file.
 *
 * @package SheKnows\Infuse\Core
 */
class UpstreamSiteConfig extends UpstreamConfig {
	/**
	 * Constructor.
	 *
	 * @param CacheInfuseJson $cache
	 */
	public function __construct( CacheInfuseJson $cache ) {
		$this->cache = $cache;
	}
}
