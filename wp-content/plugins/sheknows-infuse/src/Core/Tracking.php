<?php

namespace SheKnows\Infuse\Core;

use SheKnows\Infuse\Contracts\SubsystemTracker;
use SheKnows\Infuse\Plugin;

/**
 * Handles tracking of events and appends data
 *
 * Class Tracking
 *
 * @package SheKnows\Infuse\Core
 */
class Tracking implements SubsystemTracker {

	/**
	 * Base URL for our tracking endpoint.
	 *
	 * @var string
	 */
	protected $endpoint = 'https://api.sheknows.com/partners/infuse/logs';

	/**
	 * @var Options
	 */
	protected $options;

	/**
	 * @var Plugin
	 */
	protected $plugin;

	/**
	 * @var Container
	 */
	protected $container;

	/**
	 * Tracking constructor. Should only be called when the sheknows_track_event
	 * hook first fires.
	 *
	 * @param Options $options
	 * @param Plugin $plugin
	 * @param Container $container
	 */
	public function __construct( Options $options, Plugin $plugin, Container $container ) {
		$this->options   = $options;
		$this->plugin    = $plugin;
		$this->container = $container;
	}


	/**
	 * Collects data to append to a tracking beacon
	 *
	 * @param string $eventName  Name of tracked event
	 */
	public function trackEvent( $eventName ) {
		$body                = array( 'event_type' => $eventName );
		$subsystemTrackers   = $this->container->getCallouts( 'SubsystemTracker' );
		$subsysTrackersCount = count( $subsystemTrackers );

		for ( $i = 0; $i < $subsysTrackersCount; $i ++ ) {
			$response = $this->container->resolve( $subsystemTrackers[ $i ] )->requestTrackingData();

			foreach ( $response as $key => $value ) {
				if ( ! is_null( $value ) && '' !== $value ) {
					$body[ $key ] = $value;
				}
			}
		}

		$this->fireBeacon( $body );
	}

	/**
	 * Requests the actual tracking beacon.
	 *
	 * @param array $body POST body.
	 */
	protected function fireBeacon( $body ) {
		$headers = array( 'Content-Type' => 'application/json' );
		wp_remote_post(
			$this->endpoint,
			array(
				'body'    => \SheKnows\Infuse\json_encode( $body ),
				'headers' => $headers,
			)
		);
	}

	/**
	 * Tracking is itself a subsystem tracker. This call returns the most basic
	 * and universal tracking data that pertains to the site/wordpress system as
	 * a whole, not to any specific subsystem.
	 *
	 * @return array
	 */
	public function requestTrackingData() {
		global $wp_version;

		$theme = wp_get_theme();

		$trackingData = array(
			'infuse_plugin_version' => $this->plugin->getVersion(),
			'wordpress_version'     => $wp_version,
			'php_version'           => sprintf( '%d.%d.%d', PHP_MAJOR_VERSION, PHP_MINOR_VERSION, PHP_RELEASE_VERSION ),
			'site_url'              => get_site_url(),
			'site_name'             => get_bloginfo( 'name' ),
			'template_name'         => $theme->name ? $theme->name : $theme->get_stylesheet(),
			'parent_template'       => $theme->parent_theme,
			'boomerang_path'        => $this->options->get( 'boomerang_path' ),
		);

		return $trackingData;
	}

}
