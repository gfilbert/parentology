<?php

namespace SheKnows\Infuse\Core;

/**
 * Class AdTypes
 *
 * @package SheKnows\Infuse\Core
 */
class AdTypes {
	/**
	 * The types of ads supported by Boomerang that are selectable by sites.
	 * @var array
	 */
	protected $types = array(
		'banner'          => array(
			'friendlyName' => 'Banner',
			'sizes'        => array( '728x90' ),
		),
		'flexbanner'      => array(
			'friendlyName' => 'Flex Banner',
			'sizes'        => array( '728x90', '970x250', '970x90' ),
		),
		'medrec'          => array(
			'friendlyName' => 'Medium Rectangle',
			'sizes'        => array( '300x250' ),
		),
		'flexrec'         => array(
			'friendlyName' => 'Flex Rectangle',
			'sizes'        => array( '300x250', '300x600' ),
		),
		'tinybanner'      => array(
			'friendlyName' => 'Tiny Banner',
			'sizes'        => array( '300x50', '320x50' ),
		),
		'sky'             => array(
			'friendlyName' => 'Wide Skyscraper',
			'sizes'        => array( '160x600' ),
		),
		'mobileincontent' => array(
			'friendlyName' => 'Mobile In-content',
			'sizes'        => array( '300x250', '300x50', '320x50' ),
		),
		'wideincontent'   => array(
			'friendlyName' => 'Wide In-content',
			'sizes'        => array( '300x250', '728x90' ),
		),
		'widestincontent' => array(
			'friendlyName' => 'Widest In-content',
			'sizes'        => array( '300x250', '728x90', '970x90', '970x250' ),
		),
		'nativemini'      => array(
			'friendlyName' => 'Small Native In-Scroll',
		),
		'nativecontent'   => array(
			'friendlyName' => 'Native In-Content',
		),
		'nativesidebar'   => array(
			'friendlyName' => 'Native Sidebar',
		),
	);

	/**
	 * @var Config
	 */
	protected $config;

	/**
	 * @var ErrorLogging
	 */
	protected $errorLogging;

	/**
	 * AdTypes constructor.
	 * @param Config $config
	 */
	public function __construct( Config $config, ErrorLogging $errorLogging ) {
		$this->config       = $config;
		$this->errorLogging = $errorLogging;
		$this->applyClsOptimization();
	}

	/**
	 * Applying CLS optimization to all ad types by calculating min width & height for the div inline style
	 */
	protected function applyClsOptimization() {
		try {
			$optimizeCls = $this->config->get( 'optimize_cls', 'default_optimize_cls' );
		} catch ( \Exception $exception ) {
			$this->errorLogging->record( $exception );
			$optimizeCls = false;
		}

		// backward compatible code from existing flag = enable to be use as max size
		if ( 'enable' === $optimizeCls ) {
			$optimizeCls = 'maxsize';
		}

		if ( ! in_array( $optimizeCls, array( 'maxsize', 'minsize' ), true ) ) {
			return;
		}

		foreach ( $this->types as $key => $type ) {
			if ( empty( $type['sizes'] ) ) {
				continue;
			}
			$minWidth       = 0;
			$minHeight      = 0;
			$canOptimizeCls = false;

			foreach ( $type['sizes'] as $size ) {
				list( $width, $height ) = array_map( 'intval', explode( 'x', $size ) );

				if ( ! $canOptimizeCls && ( $width >= 50 || $height >= 50 ) ) {
					$canOptimizeCls = true;
				}

				if ( 'maxsize' === $optimizeCls ) {
					if ( $width > $minWidth ) {
						$minWidth = $width;
					}
					if ( $height > $minHeight ) {
						$minHeight = $height;
					}
				} elseif ( 'minsize' === $optimizeCls ) {
					if ( $width < $minWidth || 0 === $minWidth ) {
						$minWidth = $width;
					}
					if ( $height < $minHeight || 0 === $minHeight ) {
						$minHeight = $height;
					}
				}
			}

			if ( $canOptimizeCls && ! empty( $minWidth ) && ! empty( $minHeight ) ) {
				if ( $minWidth < 50 ) {
					$minWidth = 50;
				}
				if ( $minHeight < 50 ) {
					$minHeight = 50;
				}
				$type['style']       = sprintf( 'min-width:%dpx;min-height:%dpx', $minWidth, $minHeight );
				$this->types[ $key ] = $type;
			}
		}
	}

	/**
	 * Get the list of all ad types.
	 *
	 * @return array
	 */
	public function getAllTypes() {
		return $this->types;
	}

	/**
	 * Get the slot type definition for a single slot type by its name.
	 *
	 * @param  string $name The slot type to return the definition for.
	 * @return array|null
	 */
	public function getTypeByName( $name ) {
		if ( isset( $this->types[ $name ] ) ) {
			return $this->types[ $name ];
		}
	}
}
