<?php

namespace SheKnows\Infuse\Core;

/**
 * Handles anything that is ultimately echoed to the page (including
 * non-visible code such as header js to run Boomerang)
 *
 * Class View
 *
 * @package SheKnows\Infuse\Core
 */
class View {

	/**
	 * The base directory where views are located.
	 *
	 * @var string
	 */
	protected $baseDirectory;

	/**
	 * The name of the view that will be rendered.  This name should NOT
	 * include the .php extension.  If the view is located in a subdirectory,
	 * this should include the relative path structure, ie 'subdir/viewname'.
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * An associative array of variables to pass to the view.  These will be
	 * extracted into the view's global variables.
	 *
	 * @var array
	 */
	protected $vars;

	/**
	 * View constructor.
	 *
	 * @param string $baseDirectory  The base directory of the views. Example:
	 *                               'plugin-dir/views'
	 * @param string $name           The filename of the view to be rendered,
	 *                               relative to the views base directory and
	 *                               without the .php extension.  Example:
	 *                               'admin/settings' would result in rendering
	 *                               'plugin-dir/views/admin/settings.php'.
	 * @param array  $vars           A list of variables to be passed to the
	 *                               view.
	 */
	public function __construct( $baseDirectory, $name, $vars = array() ) {
		$this->baseDirectory = $baseDirectory;
		$this->name          = $name;
		$this->vars          = $vars;
	}

	/**
	 * Render the View and return the result.
	 *
	 * @return string
	 */
	public function render() {
		// phpcs:ignore WordPress.PHP.DontExtract
		extract( $this->vars );

		ob_start();
		include rtrim( $this->baseDirectory, '/\\' ) . '/' . $this->name . '.php';
		return ob_get_clean();
	}
}
