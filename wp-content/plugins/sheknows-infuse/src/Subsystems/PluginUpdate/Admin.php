<?php

namespace SheKnows\Infuse\Subsystems\PluginUpdate;

use SheKnows\Infuse\Core\SubsystemAdmin;

class Admin extends SubsystemAdmin {
	/**
	 * The name of the option that enables automatic updates.
	 *
	 * @var string
	 */
	const OPTION_ENABLED = 'autoupdate_enabled';

	/**
	 * Sanitize the PluginUpdate non-lazy-loaded settings.
	 *
	 * @param  array $input  The array of non-lazy-loaded options.
	 * @return array         The sanitized input values from this subsystem.
	 */
	public function sanitize( $input ) {
		$newInput = array();

		if ( isset( $input[ self::OPTION_ENABLED ] ) && $input[ self::OPTION_ENABLED ] ) {
			$newInput[ self::OPTION_ENABLED ] = true;
		} else {
			$newInput[ self::OPTION_ENABLED ] = false;
		}

		return $newInput;
	}

	/**
	 * Register the auto updates field.
	 */
	public function register() {
		$this->coreAdmin->addSettingsField( self::OPTION_ENABLED, 'general', 'Enable automatic updates', array( $this, 'autoUpdateEnabled' ) );
	}

	/**
	 * Adds the checkbox to enable/disable the automatic updates.  Unlike some
	 * other options, this option is *never* disabled.  We want users to keep
	 * the plugin up to date if it's installed regardless of whether
	 * it's actively doing anything else.
	 */
	public function autoUpdateEnabled() {
		printf(
			'<input type="checkbox" id="%s" name="%s" value="1" %s /><p><b>Recommended.</b>  If checked, Infuse will automatically update itself when new versions are available.</p>',
			esc_attr( self::OPTION_ENABLED ),
			esc_attr( $this->coreAdmin->getInputName( self::OPTION_ENABLED ) ),
			checked( $this->coreAdmin->getDefaultValue( self::OPTION_ENABLED ), true, false )
		);
	}
}
