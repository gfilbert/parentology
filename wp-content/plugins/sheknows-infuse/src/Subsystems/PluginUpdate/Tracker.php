<?php

namespace SheKnows\Infuse\Subsystems\PluginUpdate;

use SheKnows\Infuse\Core\SubsystemTracker;

class Tracker extends SubsystemTracker {

	/**
	 * Returns whether auto update is enabled.
	 *
	 * @return array
	 */
	public function requestTrackingData() {
		$trackingData = array(
			'automatic_updates_enabled' => (bool) $this->options->get( 'autoupdate_enabled' ),
		);

		return $trackingData;
	}
}
