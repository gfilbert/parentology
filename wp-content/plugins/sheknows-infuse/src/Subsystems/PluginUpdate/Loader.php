<?php

namespace SheKnows\Infuse\Subsystems\PluginUpdate;

use SheKnows\Infuse\Core\SubsystemLoader;

/**
 * Loads required classes for the plugin to update itself.
 *
 * Class Loader
 *
 * @package SheKnows\Infuse\Subsystems\PluginUpdate
 */
class Loader extends SubsystemLoader {

	/**
	 * Initializes the subsystem.
	 */
	public function load() {
		$this->bindSubsystemClasses();
		$this->registerOptions();
		$this->loadPluginUpdateManager();
	}

	/**
	 * Makes relevant classes available at runtime. If class has not been
	 * instantiated, the class is instantiated (with dependencies added through
	 * the constructor) and handed over. If the class has been instantiated,
	 * the class is handed over.
	 */
	protected function bindSubsystemClasses() {
		$this->container->bind( __NAMESPACE__ . '\PluginUpdateManager', array( $this, 'bindPluginUpdateManager' ) );
		$this->container->bind( __NAMESPACE__ . '\Admin', array( $this, 'bindAdmin' ), 'SubsystemAdmin' );
		$this->container->bind( __NAMESPACE__ . '\Tracker', array( $this, 'bindTracker' ), 'SubsystemTracker' );
	}

	/**
	 * Instantiates PluginUpdateManager.
	 *
	 * @return PluginUpdateManager
	 */
	public function bindPluginUpdateManager() {
		$options = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );
		$plugin  = $this->container->resolve( 'SheKnows\Infuse\Plugin' );

		return new PluginUpdateManager( $plugin, $options );
	}

	/**
	 * Constructs the Admin class for the container.
	 *
	 * @return Admin
	 */
	public function bindAdmin() {
		$options   = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );
		$plugin    = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$coreAdmin = $this->container->resolve( 'SheKnows\Infuse\Core\Admin' );

		return new Admin( $options, $plugin, $coreAdmin );
	}

	/**
	 * Constructs tracker class
	 *
	 * @return Tracker
	 */
	public function bindTracker() {
		$options = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );
		$plugin  = $this->container->resolve( 'SheKnows\Infuse\Plugin' );

		return new Tracker( $options, $plugin );
	}

	/**
	 * Register this subsystem's settings.
	 */
	protected function registerOptions() {
		$this->container->resolve( 'SheKnows\Infuse\Core\Options' )->register( 'autoupdate_enabled', true );
	}

	/**
	 * Loads an PluginUpdateManager instance, which kicks off checking for
	 * updates within the update-checker library.
	 */
	public function loadPluginUpdateManager() {
		$this->container->resolve( __NAMESPACE__ . '\PluginUpdateManager' )->checkPluginUpdate();
	}
}
