<?php

namespace SheKnows\Infuse\Subsystems\AdsTxt;

use SheKnows\Infuse\Core\CacheBoomerangSiteFile;

/**
 * Checks and updates cached ads.txt.
 *
 * @package SheKnows\Infuse\Subsystems\AdsTxt
 */
class Cache extends CacheBoomerangSiteFile {
	/**
	 * Portion of the transient name that uniquely identifies this cached file.
	 * Subclasses should set this.
	 *
	 * @var string
	 */
	protected $transientName = 'adstxt';

	/**
	 * {@inheritdoc}
	 */
	public function getUrl() {
		$baseUrl = $this->getBaseUrl();

		return $baseUrl ? sprintf( '%s/ads.txt', $baseUrl ) : false;
	}
}
