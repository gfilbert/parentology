<?php

namespace SheKnows\Infuse\Subsystems\Intercom;

use SheKnows\Infuse\Core\SubsystemLoader;

/**
 * Class Loader
 *
 * @package SheKnows\Infuse\Subsystems\Intercom
 */
class Loader extends SubsystemLoader {
	/**
	 * Initializes relevant WordPress API hooks.
	 */
	public function load() {
		$this->registerSubsystemHooks();
	}

	/**
	 * Intercom script ID.
	 */
	const ID = 'sheknowsInfuseIntercomScript';

	/**
	 * Intercom class.
	 */
	const INTERCOM_LAUNCHER_CLASS = 'sheknows-infuse-intercom-launcher';

	/**
	 * Adds hook to the WordPress API.
	 */
	protected function registerSubsystemHooks() {
		if ( is_admin() ) {
			add_action( 'admin_menu', array( $this, 'addIntercomMenu' ) );
			add_filter( 'sheknows_infuse_not_configured_message', array( $this, 'addIntercomMessage' ) );

			add_action( 'admin_enqueue_scripts', array( $this, 'localizeIntercomScript' ), 99 );
		}
	}

	/**
	 * Updates infuse warning message.
	 *
	 * @param string   $message
	 *
	 * @return string
	 */
	public function addIntercomMessage( $message ) {
		$message = $message . '<br>If you need assistance, please <a class="' . self::INTERCOM_LAUNCHER_CLASS . '" href="#">contact SHE Media Support</a>.';

		return $message;
	}

	/**
	 * Adds Intercom button to the top level menu.
	 */
	public function addIntercomMenu() {
		add_menu_page( 'Contact SHE Media Support', 'Contact SHE Media Support', 'edit_posts', self::INTERCOM_LAUNCHER_CLASS, '', 'dashicons-testimonial' );
	}

	/**
	 * Localizes intercom script with the required parameters.
	 */
	public function localizeIntercomScript() {
		$plugin       = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$siteConfig   = $this->container->resolve( 'SheKnows\Infuse\Core\UpstreamSiteConfig' );
		$errorLogging = $this->container->resolve( 'SheKnows\Infuse\Core\ErrorLogging' );

		$pluginSlug = $plugin->getSlug();

		try {
			$siteID = $siteConfig->get( 'site_id' );
		} catch ( \Exception $exception ) {
			$errorLogging->record( $exception );
			$siteID = 'Unknown';
		}

		$intercomOptions = array(
			'siteID' => $siteID,
		);

		wp_localize_script( $pluginSlug . '-admin', 'sheknowsInfuseIntercomOptions', $intercomOptions );
	}
}
