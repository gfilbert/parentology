<?php

namespace SheKnows\Infuse\Subsystems\HeaderCode;

use SheKnows\Infuse\Core\ErrorLogging;
use SheKnows\Infuse\Core\ViewFactory;

/**
 * Writes Boomerang header code to non-admin pages.
 *
 * Class HeaderCodeWriter
 *
 * @package SheKnows\Infuse\Subsystems\HeaderCode
 */
class HeaderCodeWriter {

	/**
	 * @var ViewFactory
	 */
	protected $viewFactory;

	/**
	 * @var ErrorLogging
	 */
	protected $errorLogging;

	/**
	 * @var DisableAds
	 */
	protected $disableAds;

	/**
	 * @var Vertical
	 */
	protected $vertical;

	/**
	 * @var Sponsored
	 */
	protected $sponsored;

	/**
	 * @param ViewFactory $viewFactory
	 * @param ErrorLogging $errorLogging
	 * @param DisableAds $disableAds
	 * @param Vertical $vertical
	 * @param Sponsored $sponsored
	 */
	public function __construct( ViewFactory $viewFactory, ErrorLogging $errorLogging, DisableAds $disableAds, Vertical $vertical, Sponsored $sponsored ) {
		$this->viewFactory  = $viewFactory;
		$this->errorLogging = $errorLogging;
		$this->disableAds   = $disableAds;
		$this->vertical     = $vertical;
		$this->sponsored    = $sponsored;
	}

	/**
	 * Returns targeting array based on current queried object.
	 *
	 * @param array $vars
	 * @return array
	 */
	private function getTargeting( $vars ) {
		$targeting = array();

		switch ( true ) {
			case is_front_page():
				$targeting['ci'] = 'front';
				$targeting['pt'] = 'home';
				break;
			case is_home():
				$targeting['ci'] = 'posts';
				$targeting['pt'] = 'home';
				break;
			case is_singular():
				$targeting['ci'] = get_queried_object_id();
				$targeting['pt'] = get_post_type();
				break;
			case is_category():
			case is_tag():
			case is_tax():
				$targeting['ci'] = 'term-' . get_queried_object_id();
				$targeting['pt'] = 'landing';
				break;
			case is_author():
				$targeting['ci'] = 'author-' . get_queried_object_id();
				$targeting['pt'] = 'author';
				break;
			case is_archive():
				$targeting['pt'] = 'landing';
				break;
			default:
				$targeting['ci'] = false;
				$targeting['pt'] = false;
		}

		if ( is_singular() || is_tag() ) {
			$tags = get_the_tags();
			if ( ! empty( $tags ) && is_array( $tags ) ) {
				$targeting['tags'] = array_map(
					function ( $tag ) {
						return $tag->slug;
					},
					$tags
				);
			}
		}

		if ( is_singular() || is_category() ) {
			$categories = get_the_category();
			if ( ! empty( $categories ) && is_array( $categories ) ) {
				$categories = array_map(
					function ( $category ) {
						return $category->slug;
					},
					$categories
				);
			}

			$targeting['ch'] = count( $categories ) > 1 ? $categories : reset( $categories );
		}

		$targeting = apply_filters( 'sheknows_infuse_global_targeting', $targeting, $vars );

		// In case a filter implementation changes the $targeting type.
		if ( ! is_array( $targeting ) ) {
			$this->errorLogging->record( new \Exception( 'Invalid data returned from sheknows_infuse_global_targeting filter.' ) );
			$targeting = array();
		}

		return array_filter( $targeting );
	}

	/**
	 * Renders the headercode view with appropriate Boomerang path for the site.
	 *
	 * @param string $boomerangPath
	 */
	public function writeHeaderCode( $boomerangPath ) {
		if ( ! empty( $boomerangPath ) ) {
			$vars = array(
				'boomerang_path' => $boomerangPath,
				'disable_ads'    => $this->disableAds->getValue(),
				'vertical'       => $this->vertical->getValue(),
				'sponsored'      => $this->sponsored->getValue(),
			);

			$vars['targeting'] = $this->getTargeting( $vars );

			// phpcs:ignore WordPress.Security.EscapeOutput
			echo $this->viewFactory->make( 'headercode', $vars )->render();
		}
	}

}
