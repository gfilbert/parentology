<?php

namespace SheKnows\Infuse\Subsystems\HeaderCode;

use SheKnows\Infuse\Core\SubsystemAdmin;

class Admin extends SubsystemAdmin {
	/**
	 * The name of the option that enables inserting the header code.
	 *
	 * @var string
	 */
	const OPTION_ENABLED = 'header_code_enabled';

	/**
	 * Sanitize the HeaderCode non-lazy-loaded settings.
	 *
	 * @param  array $input  The array of non-lazy-loaded options.
	 * @return array         The sanitized input values from this subsystem.
	 */
	public function sanitize( $input ) {
		$newInput   = array();
		$oldEnabled = $this->options->get( self::OPTION_ENABLED );

		if ( $this->coreAdmin->globallyEnabled() ) {
			if ( isset( $input[ self::OPTION_ENABLED ] ) && $input[ self::OPTION_ENABLED ] ) {
				$newInput[ self::OPTION_ENABLED ] = true;
			} else {
				$newInput[ self::OPTION_ENABLED ] = false;
			}
		} else {
			// If the Boomerang path has not been configured, this checkbox is
			// disabled, so we need to treat it specially, since both situations
			// will cause the form value to not be submitted.  Otherwise, we
			// would see the checkbox as unset, even though it was really just
			// disabled.
			$newInput[ self::OPTION_ENABLED ] = (bool) $oldEnabled;
		}

		return $newInput;
	}

	/**
	 * Register the header code insertion field, but only if we already have the
	 * Boomerang path.
	 */
	public function register() {
		if ( $this->coreAdmin->globallyEnabled() ) {
			$this->coreAdmin->addSettingsField( self::OPTION_ENABLED, 'general', 'Insert Boomerang header code', array( $this, 'insertHeaderCode' ) );
		}
	}

	/**
	 * Adds the checkbox to enable/disable the insertion of the header code.
	 */
	public function insertHeaderCode() {
		$disabled = ! $this->coreAdmin->globallyEnabled();

		printf(
			'<input type="checkbox" id="%s" name="%s" value="1" %s %s /><p><b>Recommended.</b>  If checked, Infuse will automatically inject the required Boomerang header code into your site\'s header.<br /><b>NOTE:</b> This is required to use SHE Media Ad Widgets and Shortcodes in WordPress.</p>',
			esc_attr( self::OPTION_ENABLED ),
			esc_attr( $this->coreAdmin->getInputName( self::OPTION_ENABLED ) ),
			disabled( $disabled, true, false ),
			checked( $this->coreAdmin->getDefaultValue( self::OPTION_ENABLED ), true, false )
		);
	}
}
