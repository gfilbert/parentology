<?php

namespace SheKnows\Infuse\Subsystems\AdWidget;

use SheKnows\Infuse\Core\SubsystemTracker;

class Tracker extends SubsystemTracker {

	/**
	 * Returns information on ad widgets.
	 *
	 * @return array
	 */
	public function requestTrackingData() {
		$slug          = $this->plugin->getSlug() . '-ad-widget';
		$infuseWidgets = get_option( 'widget_' . $slug );

		if ( $infuseWidgets ) {
			// Get rid of the WP API flag
			unset( $infuseWidgets['_multiwidget'] );
			$numberOfWidgets = count( $infuseWidgets );
		} else {
			$numberOfWidgets = 0;
		}

		$trackingData = array(
			'number_of_ad_widgets' => $numberOfWidgets,
		);

		return $trackingData;
	}
}
