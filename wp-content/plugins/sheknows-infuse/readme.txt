=== SHE Media Infuse ===
Tags: SHE Media, Infuse, Boomerang, advertise, advertising, monetize
Requires at least: 3.8
Tested up to: 5.8
Requires PHP: 5.3
Stable tag: 1.0.29

== Description ==

**SHE Media Infuse** simplifies management of ads and monetization for sites affiliated with SHE Media using the WordPress CMS.

== Installation ==

1. Download the latest version of the plugin from https://infuse.shemedia.com/sheknows-infuse-latest.zip
2. Go to your **Plugins** page in the WordPress admin on your site.
3. Click **Add New**
4. Click **Upload**
5. Navigate to your the location where the zip file was downloaded, and select the `sheknows-infuse-latest.zip` file.
6. Click **Activate**
7. Navigate to the settings page, located in the WordPress admin sidebar under **Settings**/**SHE Media Infuse**.
8. Click the link to set the **Boomerang path** for your site.  You may be asked to log in to your SHE Media Partner Network account.

== Ads.txt ==
By default SHE Media Infuse will pull your site's ads.txt from Boomerang and serve it for you.
Some notes on this:
1. Infuse serves your ads.txt "virtually".  If you uploaded a physical ads.txt file to your WordPress installation, Infuse will be unable to serve ads.txt until the file is removed.  If Infuse detects an ads.txt file, it will warn you and give you an option to remove it so that Infuse can serve its ads.txt entries.
2. You must have Permalinks enabled in WordPress to serve ads.txt.
3. Advertisers will look for your ads.txt file at the root of your domain (e.g. http://example.com/ads.txt).  If your WordPress installation is not served from the root of your domain, and you wish to use the Ads.txt functionality in Infuse, you will need to set up a redirect in your web server to redirect your root /ads.txt URL to the ads.txt URL served by Infuse at the root of your WordPress site.  For instance, if your WordPress site is served at http://example.com/myblog, you will need to set up a redirect from http://example.com/ads.txt to http://example.com/myblog/ads.txt in your web server software.  Please contact your hosting company if you need assistance in setting up an ads.txt redirect.
4. SHE Media Infuse will automatically keep your ads.txt file up to date with any changes provided by SHE Media.
5. If needed, you can disable ads.txt on the SHE Media Infuse settings page.  If you do this, it will be your responsibility to keep your ads.txt entries up to date with those provided by SheKnows, as Infuse will no longer be able to do that for you.
6. You can also add custom entries to the ads.txt file served by Infuse from the SHE Media Infuse settings page.

== Ad Widget ==
Ads can be placed in your sidebar and other defined regions in your template using the provided `SHE Media Ad` widget.

== Shortcodes ==
Ads can be added inline into posts with this shortcode: `[shemedia_ad type="medrec"]`. See the help tab within the post editing pages for more information on using the `shemedia_ad` shorcode.

== Changelog ==

= 1.0.29 =
* Multi-size ad containers may now reserve the minimum ad size or maximum ad size when optimized to reduce layout shift.
* Enhance cache support for contextual ad targeting data.

= 1.0.28 =
* Fix an issue where blank ad spaces are present when ads are disabled on a post and CLS optimization is enabled.

= 1.0.27 =
* Add support for optimizing ad containers to prevent shifting of content on ad load and refresh (a la Core Web Vitals/Cumulative Layout Shift).

= 1.0.26 =
* Fix a bug with retrieving contextual targeting data.

= 1.0.25 =
* Add support for additional ad types for ad widgets and shortcodes.
* Add support for pulling in contextual targeting data.
* Fix an issue with the ads.txt handler if the plugin is loaded late.
* Remove calls to the -eu version of Boomerang.
* Pull in upstream fixes and features to the automated plugin update package.
* Misc updates and fixes.

= 1.0.24 =
* Skipped.

= 1.0.23 =
* Add ability to show ADVERTISEMENT callout above ad widgets and ad shortcodes.
* Fix an issue where the ads.txt handler might error during the shutdown phase when combined with some plugins that register shutdown hooks.
* Add live assistance support.

= 1.0.22 =
* Add SHE Media assets to WP Rocket's minification exclusion lists to keep WP Rocket from breaking ad functionality.
* Pull in upstream fixes and features to the automated plugin update package.

= 1.0.21 =
* Pull in upstream fixes and features to the automated plugin update package.
* Remove support for Oriel ad block recovery.

= 1.0.20 =
* Minor bug fix.

= 1.0.19 =
* Fix an issue affecting saving of configuration options.

= 1.0.18 =
* Updates to messaging when Ad Block Recovery can't be enabled for some reason.
* Pull in upstream fixes and features to the automated plugin update package.

= 1.0.17 =
* Check for existence of the Oriel SDK before activating Infuse ad block protection, since conflicts could arise if two copies of the Oriel SDK are active simultaneously.

= 1.0.16 =
* Add ad block protection support.
* Move error log display and reconfiguration link to new Troubleshooting section on Infuse settings page.

= 1.0.15 =
* Validate lines added to Custom Ads.txt entries to ensure they are compliant with the specification.
* Add ability to mark a post as Sponsored, which only disables certain ads rather than disabling all ads.
* Add ability to override the default site vertical on a post by post basis.
* Misc updates and fixes.

= 1.0.14 =
* Fix a bug that would cause an incorrect Boomerang path to be used on some sites.

= 1.0.13 =
* All installations must now use the automatic click-to-configure option when installing Infuse.
* A new checkbox has been added to post pages to allow the publisher to disable SHE Media ads on that post.
* Infuse now stores errors it experiences so that they can be sent to Support.
* Infuse now sets "ci" and "pt" targeting out of the box for most pages.
* Publishers who are on an exclusive contract will now be notified if they are potentially in breach of their contracts.

= 1.0.12 =
* Introduce the shemedia_ad shortcode.  The existing sheknows_ad shortcode still exists and is an alias for shemedia_ad.
* Remove the main ad flags from widgets and shortcodes.

= 1.0.11 =
* The SheKnows Partner Network is now known as the SHE Media Partner Network.  Updated branding, links, etc.

= 1.0.10 =
* Update links and text to reference the newly-released Partner Network dashboard.

= 1.0.9 =
* Update URLs for one-click configuration to point to the new and improved config application.
* Minor wording updates.

= 1.0.8 =
* Bug fixes.

= 1.0.7 =
* Tracking of Infuse utilization to aid in product development and troubleshooting.

= 1.0.6 =
* Fix an issue where Infuse would request an upstream Boomerang file when it was expected that Infuse would use a cached version of the file.
* Apply a minimum cache longevity of 15 minutes for selected upstream Boomerang files.

= 1.0.5 =
* When the site is configured to require GDPR consent always, directly call the GDPR-enabled version of Boomerang.

= 1.0.4 =
* Fix an issue that was causing an Ad Widget marked as the Main Ad to lose its Main Ad status.
* Fix a couple of typos on the Options page.

= 1.0.3 =
* New one-click configuration of your Boomerang path.
* Updates to the Settings page for better UX.
* Don't require Permalinks be enabled for Ads.txt in case the site is running rewrite rules without Permalinks.  We still warn if Permalinks aren't enabled.
* Add new plugins to the list of plugins that users are warned may conflict with Infuse's ability to serve Ads.txt.

= 1.0.2 =
* Fix an issue that was keeping Infuse from properly activating on sites with PHP open_basedir restrictions.
* Fix serving of the ads.txt request on some sites that were not properly rewriting the request.
* Warn users when there could be problems serving ads.txt, such as an actual ads.txt file in the WordPress installation root.
* Provide a method for users to delete an ads.txt file from the Infuse settings page.
* Add a link to the Infuse settings page from the Plugins page entry for Infuse.
* Add a reminder for admins when Infuse has been activated but the Boomerang path has not been set.
* Add a Generator header when Infuse serves ads.txt.

= 1.0.1 =
* Fix an issue where the request for ads.txt was returning the proper content but a 404 response code.

= 1.0 =
* Initial SHE Media Infuse release
