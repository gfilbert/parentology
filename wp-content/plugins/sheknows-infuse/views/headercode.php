<?php

// Prevent direct file access
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// phpcs:disable WordPress.WP.EnqueuedResources
// phpcs:disable WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
?>
<!-- Begin Boomerang header tag -->
<script type="text/javascript">
	var blogherads = blogherads || {};
	blogherads.adq = blogherads.adq || [];

	blogherads.adq.push(function () {
	<?php if ( $disable_ads ) : ?>
		blogherads.disableAds();
	<?php endif; ?>
	<?php if ( ! empty( $sponsored ) ) : ?>
		blogherads.setSponsored(true);
	<?php endif; ?>
	<?php if ( ! empty( $vertical ) ) : ?>
		<?php // phpcs:ignore WordPress.Security.EscapeOutput ?>
		blogherads.setConf("vertical", <?php echo SheKnows\Infuse\json_encode( $vertical ); ?>);
	<?php endif; ?>
	<?php if ( ! empty( $targeting ) ) : ?>
		<?php foreach ( $targeting as $key => $value ) : ?>
			<?php // phpcs:ignore WordPress.Security.EscapeOutput ?>
			blogherads.setTargeting(<?php echo SheKnows\Infuse\json_encode( $key ); ?>, <?php echo SheKnows\Infuse\json_encode( $value ); ?>);
		<?php endforeach; ?>
	<?php endif; ?>
	});
</script>
<script type="text/javascript" async="async" data-cfasync="false" src="https://ads.blogherads.com/static/blogherads.js"></script>
<script type="text/javascript" async="async" data-cfasync="false" src="https://ads.blogherads.com/<?php echo esc_attr( trim( $boomerang_path, '/' ) ); ?>/header.js"></script>
<!-- End Boomerang header tag -->
