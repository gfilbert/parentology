<?php

// Prevent direct file access
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// phpcs:disable WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
// phpcs:disable WordPress.WP.GlobalVariablesOverride.Prohibited
?>
<p>
	Shortcodes are simple macros, surrounded by square brackets, that can be added to post content in WordPress.
	When one of these shortcodes is encountered in a post, it is replaced by the content provided by the shortcode
	provider.  General information on shortcodes can be found on the
	<a href="https://codex.wordpress.org/Shortcode" target="_blank">Shortcodes page</a> in the
	<a href="https://codex.wordpress.org/" target="_blank">WordPress Codex</a>.
</p>
<p>
	<strong>SHE Media Infuse</strong> offers a simple shortcode, <code>[shemedia_ad]</code>, that allows you to
	render ads in your posts.  In addition to posts, it can be used in the standard theme files, such as your
	theme's <code>header.php</code> file, by calling the
	<a href="https://developer.wordpress.org/reference/functions/do_shortcode/" target="_blank"><code>do_shortcode()</code></a>
	function. Calling this function in your theme files allows you to use the same familiar shortcodes to place ads
	on all of your pages in areas that aren't covered by standard WordPress Widgets, such as a leaderboard at the
	top of a page or a supplemental ad between the post content and the comments.
</p>
<h2>Required Attributes</h2>
<h3>type</h3>
<p>
	The <code>type</code> attribute specifies the type of ad that will be displayed. The following is a list of
	types that are supported by this shortcode, followed by the sizes of ad that are supported by that type:
</p>
<ul>
	<?php foreach ( $adtypes as $type ) : ?>
		<li><?php echo $type; // phpcs:ignore WordPress.Security.EscapeOutput ?></li>
	<?php endforeach; ?>
</ul>
<p>
	Example: The Shortcode <code>[shemedia_ad type="medrec"]</code> will be replaced by a 300x250 ad.
</p>
<h2>Optional Attributes</h2>
<h3>float</h3>
<p>
	The <code>float</code> attribute allows the content to flow around the ad either to the left or right.  The
	<code>float</code> attribute takes an argument of either <code>left</code> or <code>right</code>.  If
	<code>float="left"</code> is used, the ad will float on the left side of the content, allowing the content to
	flow around the right side of the ad.  Conversely, <code>float="right"</code> will cause the ad to float on
	right side of the content, and the content will flow around the left side of the ad.
</p>
<h3>divid</h3>
<p>
	By default, the <code>[shemedia_ad]</code> shortcode will automatically generate an ID for the ad's container
	<code>div</code>.  If you prefer to specify the ID of the <code>div</code>, you may pass this attribute.  For
	example, <code>[shemedia_ad type="banner" id="leaderboard-ad"]</code> will display a 728x90 ad with a custom
	<code>div</code> ID of <code>leaderboard-ad</code>.  If provided, the value must be unique.
</p>
<h3>class</h3>
<p>
	You may pass arbitrary class names to this attribute. Any values passed here will be added to the
	<code>class</code> attribute of the ad's container <code>div</code>. These can be used to provide custom styling
	for the ad in your theme's CSS.  The format for the <code>class</code> attribute is identical to that of a
	standard HTML tag.  For example, <code>class="class1 class2 class3"</code> adds the three classes
	<code>class1</code>, <code>class2</code>, and <code>class3</code> to your ad container <code>div</code>.
</p>
<h3>ad-callout</h3>
<p>
	The <code>ad-callout</code> attribute shows the <code>"ADVERTISEMENT"</code> label above ads.
	The <code>ad-callout</code> attribute takes an argument of <code>enabled</code>.
	For example: <code>[shemedia_ad type="banner" ad-callout="enabled"]</code> will display a 728x90 ad with the <code>ADVERTISMENT</code> label above it.
</p>
