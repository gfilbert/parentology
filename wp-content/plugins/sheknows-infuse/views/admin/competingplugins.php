<?php

// Prevent direct file access
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Include refresh partial.
if ( file_exists( __DIR__ . '/refresh.php' ) ) {
	include 'refresh.php';
}

// phpcs:disable WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
// phpcs:disable WordPress.WP.GlobalVariablesOverride.Prohibited
?>
<div class="wrap">
	<img src="<?php echo esc_attr( $logo ); ?>" class="sheknows-logo"/>
	<h1>SHE Media Infuse</h1>
	<?php if ( ! empty( $plugins ) ) : ?>
		<p><span class="dashicons dashicons-warning" style="color: #dc3232;"></span>&nbsp;Your contract with SHE Media requires that SHE Media is the exclusive advertising partner for your site.</p>
		<p>We've detected active plugins from other ad networks running on your site.  Please deactivate them to remain in compliance with your SHE Media contract.  If you believe you are seeing this warning in error, please <a href="https://www.shemedia.com/support/#ask-question" target="_blank">contact the Helpdesk</a>.</p>
		<p><b>NOTE:</b> If If you are a new member of the SHE Media Partner Network, this warnining can be ignored while you are transitioning from your previous ad network.  Please remember to deactivate the following <?php echo esc_html( _n( 'plugin', 'plugins', count( $plugins ), 'sheknows-infuse' ) ); ?> once you have completed your transition.</p>
		<p>
		<table>
			<?php foreach ( $plugins as $slug => $plugin ) : ?>
				<?php
				$deactivationLink = add_query_arg(
					array(
						'page'   => $page_slug,
						'slug'   => $slug,
						'action' => 'deactivate',
						'nonce'  => $nonce,
					),
					admin_url( 'admin.php' )
				);
				?>
				<tr>
					<td><b><?php echo esc_html( $plugin['Name'] ); ?></b> (v<?php echo esc_html( $plugin['Version'] ); ?>)</td>
					<td><a href="<?php echo esc_url( $deactivationLink ); ?>">Deactivate</a></td>
				</tr>
			<?php endforeach; ?>
		</table>
		<?php
		$deactivationAllLink = add_query_arg(
			array(
				'page'   => $page_slug,
				'slug'   => 'all',
				'action' => 'deactivate',
				'nonce'  => $nonce,
			),
			admin_url( 'admin.php' )
		);
		?>
		<p><a href="<?php echo esc_url( $deactivationAllLink ); ?>">Deactivate All</a></p>
	<?php else : ?>
		<h3>Plugins from competing ad networks were successfully disabled.</h3>
	<?php endif; ?>
</div>
