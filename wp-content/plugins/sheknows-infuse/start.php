<?php

namespace SheKnows\Infuse;

use SheKnows\Infuse\Core\Container;
use SheKnows\Infuse\Core\Loader;

// Prevent direct file access
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Set up our autoloader.
require_once __DIR__ . '/vendor/autoload.php';

// Load our namespaced helper functions.
require_once __DIR__ . '/src/functions.php';

// Note: this is a namespaced function.
function start() {
	$container = new Container();

	$container->bind(
		'SheKnows\Infuse\Plugin',
		function () {
			return new Plugin( 'SHE Media Infuse', SHEKNOWS_INFUSE_VERSION, 'sheknows-infuse', SHEKNOWS_INFUSE_PATH );
		}
	);

	$loader = new Loader( $container, __DIR__ . '/src/Subsystems' );
	$loader->load();
}

start();
