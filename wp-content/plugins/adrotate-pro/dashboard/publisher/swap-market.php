<?php
/* ------------------------------------------------------------------------------------
*  COPYRIGHT AND TRADEMARK NOTICE
*  Copyright 2008-2019 Arnan de Gans. All Rights Reserved.
*  ADROTATE is a registered trademark of Arnan de Gans.

*  COPYRIGHT NOTICES AND ALL THE COMMENTS SHOULD REMAIN INTACT.
*  By using this code you agree to indemnify Arnan de Gans from any
*  liability that might arise from it's use.
------------------------------------------------------------------------------------ */
?>

<?php

$adrotate_swap_adverts[] = array(
	'id' => '1', //campaign id
	'type' => 'adrotatefree', //adrotatepro | adrotatefree | paid (not implemented)
	'user' => array(
		'title' => 'Floating Coconut', // blogname
		'description' => 'Moving Abroad',  // blogdescription
		'note' => 'Thank you for choosing my campaign!',
		'instance' => '12instance34' //user instance (license/generated)
	),
	'campaign' => array(
		'banner' => 'https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=2279696',
		'url' => 'https://www.floatingcoconut.net',
		'width' => '468',
		'height' => '60',
		'expires' => current_time('timestamp'),
		'category' => '1',
		'language' => 'en',
		'location' => 'worldwide'
	)
);

?>

<div class="wp-filter">
	<ul class="filter-links">
		<li><a href="<?php echo admin_url('admin.php?page=adrotate-swap&tab=market&sub=all'); ?>" <?php echo ($active_sub == 'all') ? 'class="current"' : ''; ?>>Available</a> </li>
		<li><a href="<?php echo admin_url('admin.php?page=adrotate-swap&tab=market&sub=current'); ?>" <?php echo ($active_sub == 'current') ? 'class="current"' : ''; ?>>Current</a></li>
		<li><a href="<?php echo admin_url('admin.php?page=adrotate-swap&tab=market&sub=history'); ?>" <?php echo ($active_sub == 'history') ? 'class="current"' : ''; ?>>History</a></li>
	</ul>|
	<ul class="filter-links">
		<li>Filter: </li>
		<li><form><select name=""><option value="">Category</option></select><select name=""><option value="">Location</option></select><select name=""><option value="">Language</option></select></form></li>
	</ul>
</div>

<br class="clear">

<form id="campaign-market" method="post">

	<div class="wp-list-table widefat campaign-market">

		<?php
		if(count($adrotate_swap_adverts) > 0) {
			foreach($adrotate_swap_adverts as $swap_advert) {
		?>
		<div class="campaign-card campaign-card-contact-form-7">
			<div class="campaign-card-top">
				<div class="name column-name">
					<h3>
						<?php echo $swap_advert['user']['title'];?> <a href="<?php echo $swap_advert['campaign']['banner'];?>" class="goosebox" title="Click for full preview"><img src="<?php echo $swap_advert['campaign']['banner'];?>" class="campaign-icon" alt=""></a>
					</h3>
				</div>
				<div class="action-links">
					<ul class="campaign-action-buttons">
						<li><a class="install-now button" href="<?php echo admin_url('admin.php?page=adrotate-swap&tab=market&sub=current'); ?>&id=<?php echo $swap_advert['id'];?>">Activate Now</a></li>
					</ul>
				</div>
				<div class="desc column-description">
					<p><?php echo $swap_advert['user']['description'];?></p>
					<p><?php echo $swap_advert['user']['note'];?></p>
				</div>
			</div>
			<div class="campaign-card-bottom">
				<div class="column-size"><strong>Size:</strong> <?php echo $swap_advert['campaign']['width'];?>x<?php echo $swap_advert['campaign']['height'];?> (match groups?)</div>
				<div class="column-duration"><strong>Expires:</strong> <?php echo date_i18n('D, j F Y', $swap_advert['campaign']['expires']);?></div>
				<div class="column-category"><strong>Category:</strong> <?php echo $swap_advert['campaign']['category'];?></div>
				<div class="column-language"><strong>Language:</strong> <?php echo $swap_advert['campaign']['language'];?></div>
				<div class="column-location"><strong>Location:</strong> <?php echo $swap_advert['campaign']['location'];?></div>
				<div class="column-report"><em>Report</em></div>
			</div>
		</div>
		<?php
			}
		} else {
		?>
		<p><?php _e('No campaigns available. Try again later.', 'adrotate-pro'); ?></p>
		<?php } ?>
	</div>
</form>